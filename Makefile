# -*- coding: utf-8 -*-
# :Project:   metapensiero.deform.semantic_ui -- Replace standard Deform widgets with Semantic-UI equivalent
# :Created:   Fri 16 Feb 2018 17:21:24 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Lele Gaifax
#

all: help

include Makefile.release
