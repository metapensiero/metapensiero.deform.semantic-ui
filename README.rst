.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.deform.semantic_ui -- Semantic-UI based Deform widgets
.. :Created:   Fri 16 Feb 2018 17:21:24 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2018 Lele Gaifax
..

=================================
 metapensiero.deform.semantic_ui
=================================

Replace standard Deform widgets with Semantic-UI equivalents
============================================================

:author: Lele Gaifax
:contact: lele@metapensiero.it
:license: GNU General Public License version 3 or later

This package replaces standard Deform_ Bootstrap_ based widgets with `Semantic-UI`_
equivalents.

.. _deform: https://pypi.python.org/pypi/deform
.. _bootstrap: http://getbootstrap.com/
.. _semantic-ui: https://semantic-ui.com
