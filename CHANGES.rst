.. -*- coding: utf-8 -*-

Changes
-------

0.9 (2020-08-23)
~~~~~~~~~~~~~~~~

- Align with Deform 2.0.12, add autofocus option to most fields


0.8 (2019-10-08)
~~~~~~~~~~~~~~~~

- Align with Deform 2.0.8, add HTML5 attributes to buttons


0.7 (2019-06-23)
~~~~~~~~~~~~~~~~

- Fix typo in an inline style

- Use twine to upload to PyPI


0.6 (2019-06-23)
~~~~~~~~~~~~~~~~

- Stylistic tweaks, in particular:

  - use Semantic-UI's popups instead of HTML5 title, for easier readability of long
    descriptions
  - unclutter sequences, removing redundant segments


0.5 (2018-11-26)
~~~~~~~~~~~~~~~~

- Unbreak checkbox


0.4 (2018-11-16)
~~~~~~~~~~~~~~~~

- Align with Deform 2.0.7, in particular:

  - add `tags` option support to `Select2Widget`
  - add support for HTML5 attributes


0.3 (2018-04-20)
~~~~~~~~~~~~~~~~

- Better templates for CheckboxWidget and CheckboxChoiceWidget


0.2 (2018-03-10)
~~~~~~~~~~~~~~~~

- Minor tweaks to the form's buttons, aligning them to the right and using new "link" type
  introduced with Deform 2.0.5

- Better template for DatePartsWidget

- Remove field description, it appeared twice


0.1 (2018-02-16)
~~~~~~~~~~~~~~~~

- First release


0.0 (unreleased)
~~~~~~~~~~~~~~~~

- Initial effort
